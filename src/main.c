#include "raylib.h"
#include "raymath.h"
#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdlib.h>

#define SCENERY_HIGHT -70

typedef struct Collider {
  bool disabled;
  Rectangle rect;
  bool isTrigger;
} Collider;

typedef struct Player {
  Vector2 position;
  float speed;
  bool speedBonus;
  Collider collider;
  bool kick;
  int currentAnim;
  bool changeDirection;
  int movimentDirection;
} Player;
#define PLAYER_ANIM_STOPED 0
#define PLAYER_ANIM_WALKING 1
#define PLAYER_ANIM_KICK 2
#define PLAYER_HOR_SPD 84.0f
#define MAX_FRAME_SPEED 15
#define MIN_FRAME_SPEED 1
#define PLAYER_RIGHT_MOVIMENT 1
#define PLAYER_LEFT_MOVIMENT -1
bool playerKickButton = false;
bool playerMoveButton = false;

typedef struct Prop {
  bool disabled;
  Vector2 position;
  Collider collider;
  int type;
  bool alreadyColored;
  float colorCount;
  bool kicked;
} Prop;

Prop *props;
#define PROPS_LENGTH 50
#define PROP_CAN 1
#define PROP_CARDBOARD_BOX 2
#define PROP_BOTTLE 3

typedef struct ScoreInfo {
  bool disabled;
  Vector2 position;
  int type;
  float count;
} ScoreInfo;

ScoreInfo *scoreInfos;
#define SCORE_INFO_LENGTH 80
#define SCORE_INFO_MORE 1
#define SCORE_INFO_LESS 2
#define SCORE_INFO_LESS_TEN 3
#define SCORE_INFO_FASTER 4
#define SCORE_INFO_SLOWER 5

int populationScore = 0;
bool pause = true;
bool pauseAudio = false;
float gameSpeed = 300.0f;
int gameCount = 3;
int gameOverFade = 255;
bool gameOver = false;
float gameOverCount = 0.0f;
bool showCredits = false;

bool isColliding(Collider *colA, Collider *colB);
void UpdatePlayer(Player *player, Prop *props, int propsLength, float delta, Sound fxOkay, Sound fxError, Sound fxKick, Sound fxAuuu, float *gameMusicSpeed, float *kickRingBarStartAngle);
void UpdateCameraCenter(Camera2D *camera, Player *player, float delta, int width, int height);
void UpdateCameraCenterInsideMap(Camera2D *camera, Player *player, float delta, int width, int height);
Prop *getProp();
Prop *getBottle ();
void resetGame(Player *player, float *delayFxWheels, float *gameMusicSpeed, float *kickRingBarStartAngle, float *bottleCount);
ScoreInfo *getScoreInfo();
int getRandomSpawnPosX(int backgroundFrame);
Rectangle flipHRect(Rectangle rect);
bool isCollidingKickButton(Vector2 pos);
bool isCollidingMoveButton(Vector2 pos);
bool mouseOrTouchKickButton();
bool mouseOrTouchMoveButton();
void speedFeedback(Player *player, int feedbackType);

int main(void) {
  const int screenWidth = 1280;
  const int screenHeight = 720;
  InitWindow(screenWidth, screenHeight, "raylib [core] example - 2d camera");
  srand(time(NULL));
  // Audio
  InitAudioDevice();
  Sound fxWheels = LoadSound("assets/audios/meggiepie--squeaky-wheels.wav");
  Sound fxOkay = LoadSound("assets/audios/scrampunk--okay.wav");
  Sound fxError = LoadSound("assets/audios/huminaatio--7-error.wav");
  Sound fxGameSpeedUp = LoadSound("assets/audios/gordeszkakerek--pick-up-or-found-it-secret-item.ogg");
  Sound fxGameOver = LoadSound("assets/audios/matrixxx--game-over-04.wav");
  Sound fxKick = LoadSound("assets/audios/dwsd--kick-gettinglaid.wav");
  Sound fxAuuu = LoadSound("assets/audios/jorickhoofd--low-long-scream.wav");
  SetSoundVolume(fxWheels, 1.0f);
  SetSoundVolume(fxOkay, 1.0f);
  SetSoundVolume(fxError, 1.0f);
  SetSoundVolume(fxGameSpeedUp, 1.0f);
  SetSoundVolume(fxGameOver, 5.0f);
  SetSoundVolume(fxKick, 1.5f);
  SetSoundVolume(fxAuuu, 0.8f);
  float delayFxWheels = 200.0f;
  Music musicCity = LoadMusicStream("assets/audios/inspectorj--ambience-london-street-a.ogg");
  Music music = LoadMusicStream("assets/audios/freedp-Kevin Macleod--Comic Game Loop - Mischief.mp3");
  PlayMusicStream(musicCity);
  PlayMusicStream(music);
  SetMusicVolume(music, 1.0f);
  float gameMusicSpeed = 1.0f;
  // Player
  Player player = {0};
  // Camera
  Camera2D camera = {0};
  camera.target = player.position;
  camera.offset = (Vector2){ screenWidth/2.0f, screenHeight/2.0f };
  camera.rotation = 0.0f;
  camera.zoom = 1.0f;
  // Player animation
  Texture2D playerAnim = LoadTexture("assets/animations/walking-colored.png");
  Rectangle frameRec = { 0.0f, 0.0f, (float)playerAnim.width/6, (float)playerAnim.height };
  int currentFrame = 0;
  int framesCounter = 0;
  int framesSpeed = 6;
  // Floor Texture
  Texture2D floorTexture = LoadTexture("assets/textures/floor.png");
  // Background Texture
  Texture2D backgroundTexture = LoadTexture("assets/textures/background.png");
  Texture2D backgroundLightLeftTexture = LoadTexture("assets/textures/background-light-left.png");
  Texture2D backgroundLightCenterTexture = LoadTexture("assets/textures/background-light-center.png");
  Texture2D backgroundLightRightTexture = LoadTexture("assets/textures/background-light-right.png");
  // GUI Texture
  Texture2D populationIconTexture = LoadTexture("assets/gui/population-icon.png");
  Texture2D footIconTexture = LoadTexture("assets/gui/foot-icon.png");
  Texture2D rightIconTexture = LoadTexture("assets/gui/right-icon.png");
  Texture2D menuTexture = LoadTexture("assets/gui/menu.png");
  // Props Texture
  Texture2D canTexture = LoadTexture("assets/textures/can.png");
  Texture2D cardboardBoxTexture = LoadTexture("assets/textures/cardboard-box.png");
  Texture2D bottleTexture = LoadTexture("assets/textures/bottle.png");
  // Props spawn
  float propSpawnCount = gameSpeed;
  float backgroundCount = gameSpeed*0.7;
  int backgroundFrame = 0;
  float bottleCount = 300.0f;
  props = (Prop*) malloc(sizeof(Prop)*PROPS_LENGTH);
  int rdProp;
  for(int i = 0; i < PROPS_LENGTH; i++) {
    rdProp = rand() % 1000;   
    Collider propCollider = {
      false,
      {0 , 0, 0, 0},
      true
    };
    Prop prop = {
      true,
      {0, 0},
      propCollider,
      0,
      false,
      100.0f,
      false
    };
    if(i >= PROPS_LENGTH - 10) {
      prop.collider.rect.y = 565 + 10 + SCENERY_HIGHT;
      prop.collider.rect.width = 60; 
      prop.collider.rect.height = 60;
      prop.position.y = 565 + SCENERY_HIGHT;
      prop.type = PROP_BOTTLE;
      props[i] = prop;
    }else if(rdProp >= 500) {
      prop.collider.rect.y = 565 + 10 + SCENERY_HIGHT;
      prop.collider.rect.width = 60; 
      prop.collider.rect.height = 60;
      prop.position.y = 565 + SCENERY_HIGHT;
      prop.type = PROP_CAN;
      props[i] = prop;
    } else {
      prop.collider.rect.y = 525 + 10 + SCENERY_HIGHT;
      prop.collider.rect.width = 100; 
      prop.collider.rect.height = 100;
      prop.position.y = 525 + SCENERY_HIGHT;
      prop.type = PROP_CARDBOARD_BOX;
      props[i] = prop;
    }
  }
  // ScoreInfo
  scoreInfos = (ScoreInfo*) malloc(sizeof(ScoreInfo)*SCORE_INFO_LENGTH);
  for(int i = 0; i < SCORE_INFO_LENGTH; i++) {
    ScoreInfo scoreInfo = {
      true,
      {0, 0},
      0,
      500.0f
    };
    scoreInfos[i] = scoreInfo;    
  }
  // GUI
  Vector2 kickRingBarPosition = {GetScreenWidth() - 130, GetScreenHeight() - 80};
  float kickRingBarInnerRadius = 46.0f;
  float kickRingBarOuterRadius = 65.0f;
  float kickRingBarStartAngle = 0.0f;
  float kickRingBarEndAngle = 10.0f;
  // Init Game
  resetGame(&player, &delayFxWheels, &gameMusicSpeed, &kickRingBarStartAngle, &bottleCount);
  // Tutorial
  bool showStory = true;
  bool showTutorial = false;
  int tutorialCounter = 0;
  Prop *tutorialProp = NULL;
  bool tutorialKick = true;
  Prop *tutorialBottle = NULL;
  int tutorialScoreCounter = 0;
  player.position.x = 1280;
  // ----------------------------------------------------
  // ----------- Main game loop -------------------------
  // ----------------------------------------------------
  SetTargetFPS(60);
  while (!WindowShouldClose()) {
    float deltaTime = GetFrameTime();
    UpdateMusicStream(musicCity); 
    UpdateMusicStream(music);
    SetExitKey(KEY_ESCAPE);
    // Pause
    if(IsKeyPressed(KEY_P)) pause = !pause;
    if(pauseAudio) {
      PauseMusicStream(musicCity);
      PauseMusicStream(music);
    } else {
      ResumeMusicStream(musicCity);
      ResumeMusicStream(music);
    }
    // Game over
    if(populationScore < 0) {
      if(!pauseAudio) PlaySound(fxGameOver);
      gameOver = true;
      resetGame(&player, &delayFxWheels, &gameMusicSpeed, &kickRingBarStartAngle, &bottleCount);
    }
    if(gameOver && gameOverCount >= 1.0f) {
      gameOverFade--;
      gameOverCount = 0.0f;
    }
    if(gameOverFade <= 0) {
      gameOver = false;
      gameOverFade = 255;
    }
    gameOverCount++;
    // Player collider
    if(player.movimentDirection == PLAYER_LEFT_MOVIMENT)
      player.collider.rect.x = player.position.x + 30;
    else
      player.collider.rect.x = player.position.x + 530;
    player.collider.rect.y = player.position.y + 130 + SCENERY_HIGHT;
    //Player Animation
    if((player.currentAnim == PLAYER_ANIM_WALKING && !pause) ||
       (player.currentAnim == PLAYER_ANIM_WALKING && showTutorial)) {
      // Audio
      if(delayFxWheels >= 200.0f) {
	if(!pauseAudio) PlaySound(fxWheels);
	delayFxWheels = 0.0f;
      }
      delayFxWheels++;
      framesCounter++;
      if (framesCounter >= (60/framesSpeed)) {
	framesCounter = 0;
	currentFrame++;
	if (currentFrame > 5) currentFrame = 0;
	frameRec.x = (float)currentFrame*(float)playerAnim.width/6;
      }
      if (framesSpeed > MAX_FRAME_SPEED) framesSpeed = MAX_FRAME_SPEED;
      else if (framesSpeed < MIN_FRAME_SPEED) framesSpeed = MIN_FRAME_SPEED;
    } else {
      frameRec.x = 3.0f*(float)playerAnim.width/6;
      StopSound(fxWheels);
    }
    if(player.currentAnim == PLAYER_ANIM_KICK)
      frameRec.x = 0.0f*(float)playerAnim.width/6;
    // Player update
    if(!pause)
      UpdatePlayer(&player, props, PROPS_LENGTH, deltaTime, fxOkay, fxError, fxKick, fxAuuu, &gameMusicSpeed, &kickRingBarStartAngle);
    UpdateCameraCenter(&camera, &player, deltaTime, screenWidth, screenHeight);
    // Props spawn
    if(!pause) {
      if(propSpawnCount >= gameSpeed) {
	int rdBackground;
	rdBackground = rand() % 4;
	backgroundFrame = rdBackground;
	propSpawnCount = 0.0f;
      }
      propSpawnCount++;
      if(backgroundFrame > 0) backgroundCount--;
      if(backgroundCount <= 0.0f) {
	Prop *prop = getProp();
	if(prop != NULL) {
	  int rdPos = getRandomSpawnPosX(backgroundFrame);
	  prop->collider.rect.x = rdPos + 20;
	  prop->position.x = rdPos;
	  prop->disabled = false;
	  prop->alreadyColored = true;
	  prop->colorCount = 100.0f;
	}
	backgroundFrame = 0;
	gameCount--;
	backgroundCount = gameSpeed*0.7;     
      }
    }
   if(populationScore >= 16 && bottleCount >= 500.0f) {
     Prop *prop = getBottle();
     if(prop != NULL) {
       int rdPos = getRandomSpawnPosX(backgroundFrame);
       prop->collider.rect.x = rdPos + 20;
       prop->position.x = rdPos;
       prop->disabled = false;
       prop->alreadyColored = true;
       prop->colorCount = 200.0f;       
     }
     bottleCount = 0.0f;
   }
   bottleCount++;
   // Prop kicked
   for(int i = 0; i < PROPS_LENGTH; i++) {
     Prop *prop = &props[i];
     if(!prop->disabled && prop->kicked) {
       prop->collider.rect.x += 6 * player.movimentDirection;
       prop->collider.rect.y -= 6;
       prop->position.x += 6 * player.movimentDirection;
       prop->position.y -= 6;
       if((prop->position.x <= -100 || prop->position.x >= 1280) || prop->position.y <= -100) {
	 if(prop->type == PROP_CAN) {
	   prop->collider.rect.y = 565 + 10 + SCENERY_HIGHT;
	   prop->position.y = 565 + SCENERY_HIGHT;
	 } else if(prop->type == PROP_CARDBOARD_BOX) {
	   prop->collider.rect.y = 525 + 10 + SCENERY_HIGHT;
	   prop->position.y = 525 + SCENERY_HIGHT;
	 }
	 prop->disabled = true;
	 prop->kicked = false;
       }
     }
   }
   // ScoreInfo Texts
   for(int i = 0; i < SCORE_INFO_LENGTH; i++) {
     ScoreInfo *scoreInfo = &scoreInfos[i];
     if(!scoreInfo->disabled) {
       if(scoreInfo->count <= 0) {
	 scoreInfo->count = 500.0f;
	 scoreInfo->disabled = true;
       }
       scoreInfo->position.y--;
       scoreInfo->count--;
     }
   }
   // Game speed
   if(gameCount <= 0) {
     if(gameSpeed >= 0.0f) gameSpeed -= 48.0f;
     if(gameSpeed <= 0.0f) gameSpeed = 28.0f;
     if(!pauseAudio) PlaySound(fxGameSpeedUp);
     if(gameMusicSpeed <= 1.3f) gameMusicSpeed += 0.03f;
     speedFeedback(&player, SCORE_INFO_FASTER);
     SetMusicPitch(music, gameMusicSpeed);
     gameCount = 3;
   }
   // Kick ring bar progress
   if(kickRingBarStartAngle >= 360.0f) {
     player.changeDirection = true;
   } else if(kickRingBarStartAngle >= 30.0f) {
     player.kick = true;
   } else {
     player.kick = false;
     player.changeDirection = false;
   }
   // ----------------------------------------------------
   // --------------- Draw -------------------------------
   // ----------------------------------------------------
   BeginDrawing();
   ClearBackground((Color){188, 190, 192, 255});
   // Background
   if(backgroundFrame == 0)
     DrawTextureEx(backgroundTexture, (Vector2){0, 0}, 0.0f, 1.8f, WHITE);
   else if(backgroundFrame == 1)
     DrawTextureEx(backgroundLightLeftTexture, (Vector2){0, 0}, 0.0f, 1.8f, WHITE);
   else if(backgroundFrame == 2)
     DrawTextureEx(backgroundLightCenterTexture, (Vector2){0, 0}, 0.0f, 1.8f, WHITE);
   else if(backgroundFrame == 3)
     DrawTextureEx(backgroundLightRightTexture, (Vector2){0, 0}, 0.0f, 1.8f, WHITE); 
   // Floor
   DrawTextureEx(floorTexture, (Vector2){2, SCENERY_HIGHT}, 0.0f, 0.72f, WHITE);
   // Props
   for(int i = 0; i < PROPS_LENGTH; i++) {
     Prop *prop = &props[i];
     Color color;
     if(prop->alreadyColored)
       prop->colorCount--;
     if(prop->colorCount <= 0.0f)
       color = WHITE;
     else
       color = GREEN;
     if(!prop->disabled) {
       if(prop->type == PROP_CAN)
	   DrawTextureEx(canTexture, prop->position, 0.0f, 0.4f, color);
       else if(prop->type == PROP_CARDBOARD_BOX)
	 DrawTextureEx(cardboardBoxTexture, prop->position, 0.0f, 0.5f, color);
       else if(prop->type == PROP_BOTTLE)
	 DrawTextureEx(bottleTexture, prop->position, 0.0f, 0.5f, color);
     }
   }
   // ScoreInfo Texts
   for(int i = 0; i < SCORE_INFO_LENGTH; i++) {
     ScoreInfo *scoreInfo = &scoreInfos[i];
     if(!scoreInfo->disabled) {
       if(scoreInfo->type == SCORE_INFO_MORE)
	 DrawText("+1", scoreInfo->position.x + 30, scoreInfo->position.y, 30, GREEN);
       else if(scoreInfo->type == SCORE_INFO_LESS)
	 DrawText("-1", scoreInfo->position.x + 30, scoreInfo->position.y, 30, RED);
       else if(scoreInfo->type == SCORE_INFO_LESS_TEN)
	 DrawText("-10", scoreInfo->position.x + 30, scoreInfo->position.y, 56, RED);
       else if(scoreInfo->type == SCORE_INFO_FASTER)
	 DrawText("Faster", scoreInfo->position.x + 30, scoreInfo->position.y, 22, BLUE);
       else if(scoreInfo->type == SCORE_INFO_SLOWER)
	 DrawText("Slower", scoreInfo->position.x + 30, scoreInfo->position.y, 22, DARKBROWN);
     }
   }
   // Player
   if(player.movimentDirection == PLAYER_LEFT_MOVIMENT)
     DrawTextureRec(playerAnim, frameRec, (Vector2){player.position.x - 20, player.position.y + 40 + SCENERY_HIGHT}, WHITE);
   else
     DrawTextureRec(playerAnim, flipHRect(frameRec), (Vector2){player.position.x - 20, player.position.y + 40 + SCENERY_HIGHT}, WHITE);
   // Game over
   if(gameOver) {
     DrawRectangleRec((Rectangle){0, 0, 1280, 720}, (Color){17, 17, 17, gameOverFade});
     DrawText("GAME OVER", 1280/2 - 320, 720/2 - 100, 100, (Color){126, 25, 27, gameOverFade}); 
   }
   // --------- HUD ------------
   DrawTextureEx(populationIconTexture, (Vector2){10, 10}, 0.0f, 1.0f, WHITE);
   DrawText(TextFormat("%i", populationScore), 115, 40, 40, MAROON);
   if(player.changeDirection)
     if(player.movimentDirection == PLAYER_LEFT_MOVIMENT) {
       DrawTextureEx(rightIconTexture, (Vector2){1280 - 179, 720 - 132}, 0.0f, 1.0f, MAROON);
     } else {
       DrawTextureRec(rightIconTexture, (Rectangle){0, 0, rightIconTexture.width*-1, rightIconTexture.height}, (Vector2){1280 - 179, 720 - 132}, MAROON);
     }
   else if(player.kick)
     DrawTextureEx(footIconTexture, (Vector2){1280 - 180, 720 - 130}, 0.0f, 1.0f, MAROON);
   else
     DrawTextureEx(footIconTexture, (Vector2){1280 - 180, 720 - 130}, 0.0f, 1.0f, WHITE);
   DrawRing(kickRingBarPosition, kickRingBarInnerRadius, kickRingBarOuterRadius, kickRingBarStartAngle, kickRingBarEndAngle, 0, Fade(MAROON, 0.9));
   DrawRingLines(kickRingBarPosition, kickRingBarInnerRadius, kickRingBarOuterRadius, 0, 360, 0, Fade(BLACK, 0.4));
   if(player.movimentDirection == PLAYER_LEFT_MOVIMENT)
     DrawTextureRec(rightIconTexture, (Rectangle){0, 0, rightIconTexture.width*-1, rightIconTexture.height}, (Vector2){0 + 40, 720 - 126}, MAROON);
   else
     DrawTextureRec(rightIconTexture, (Rectangle){0, 0, rightIconTexture.width, rightIconTexture.height}, (Vector2){0 + 40, 720 - 126}, MAROON);
   // GUI Style
   GuiSetStyle(DEFAULT, TEXT_COLOR_NORMAL, 0xFFFAF0FF); 
   GuiSetStyle(DEFAULT, BORDER_COLOR_NORMAL, 0xFFFAF0FF);
   GuiSetStyle(DEFAULT, BASE_COLOR_NORMAL, 0x005516FF); 
   GuiSetStyle(DEFAULT, TEXT_SIZE, 19);
   GuiSetStyle(DEFAULT, TEXT_SPACING, 3);
   GuiSetStyle(DEFAULT, BASE_COLOR_PRESSED, 0x0E6882FF);
   GuiSetStyle(DEFAULT, BASE_COLOR_FOCUSED, 0x095065FF);
   // Credits
   if(showCredits) {
     const char message[750] = "Direção, Game Design, Artes e programação: \n             Diogo Hartuiq Debarba. \nSons do freesound.com, @<autor>--<music-name>: \ndwsd--kick-gettinglaid.wav \n'freedp-Kevin Macleod--Comic Game Loop - Mischief.mp3' \ngordeszkakerek--pick-up-or-found-it-secret-item.ogg \nhuminaatio--7-error.wav \ninspectorj--ambience-london-street-a.ogg        Referências do flickr, <image-name>-flickr-<autor>:\ninspectorj--ambience-london-street-a.wav             Recycling-flickr-Tianjia Liu.jpg\nmatrixxx--game-over-04.wav \nmatrixxx--stuck-reception-desk-bell.wav \nmboscolo--bike-wheel-3.mp3 \nmboscolo--bike-wheel-3.wav \nmeggiepie--squeaky-wheels.wav \noriginal-sound--confirmation-upward.wav \nscrampunk--okay.wav \njorickhoofd--low-long-scream.wav";
     tutorialCounter += 8;
     DrawRectangleRec((Rectangle){0, 0, 1280, 720}, (Color){17, 17, 17, 230});
     DrawTextureEx(menuTexture, (Vector2){0, 0}, 0.0f, 1.0f, WHITE);     
     DrawText(TextSubtext(message, 0, tutorialCounter/10), 350, 310, 13, RAYWHITE);
     if(tutorialCounter > 9000) {
       tutorialCounter = 0;
       showCredits = false;
     }
   }
   // Story and tutorial
   if(showStory) {
     const char message[397] = "    Você é um catador de lixo de uma cidade grande \nque está com sua população crescendo e muitas das \npessoas não são tão educadas e jogam lixo no chão. \n    Você gosta de chutar os lixos de volta para elas, \nalém de coletar os lixos enquanto elas ainda olham para \nvocê, assim deixando-as envergonhadas e aumentando \na população de pessoas educadas da cidade.\n";
     tutorialCounter += 6;
     DrawRectangleRec((Rectangle){0, 0, 1280, 720}, (Color){17, 17, 17, 230});
     DrawTextureEx(menuTexture, (Vector2){0, 0}, 0.0f, 1.0f, WHITE);     
     DrawText(TextSubtext(message, 0, tutorialCounter/10), 350, 310, 22, BLACK);
     if(tutorialCounter > 8000) {
       tutorialCounter = 0;
       showStory = false;
       showTutorial = true;
     }
       if(GuiButton((Rectangle){1280 - 180, 720 - 60, 150, 40}, "PULAR")) {
       tutorialCounter = 0;
       showStory = false;
       showTutorial = true;
     }	  
   }
   if(showTutorial) {
     tutorialCounter++;
     if(player.movimentDirection == PLAYER_LEFT_MOVIMENT)
       player.position.x -= PLAYER_HOR_SPD*player.speed*deltaTime;
     else if(player.movimentDirection == PLAYER_RIGHT_MOVIMENT)
       player.position.x += PLAYER_HOR_SPD*player.speed*deltaTime;
     if(tutorialProp == NULL) {
       tutorialProp = getProp();
       if(tutorialProp != NULL) {
	 tutorialProp->collider.rect.x = 1280/2 - 50;
	 tutorialProp->position.x = 1280/2;
	 tutorialProp->disabled = false;
	 tutorialProp->alreadyColored = true;
	 tutorialProp->colorCount = 100.0f;
	 kickRingBarStartAngle += 100.0f;
       }
     }
     if(tutorialCounter >= 200) {
       if(GuiButton((Rectangle){1280 - 180, 40, 150, 40}, "PULAR")) {
	 resetGame(&player, &delayFxWheels, &gameMusicSpeed, &kickRingBarStartAngle, &bottleCount);
	 tutorialCounter = 0;
	 GuiSetStyle(DEFAULT, TEXT_SIZE, 17);
	 tutorialProp->disabled = true;
	 showTutorial = false;
       }
     }
     if(tutorialCounter >= 0 && tutorialCounter <= 136) {
       DrawText("Ande para a esquerda com a seta (<-) do teclado.", 350, 275, 23, MAROON);
       player.currentAnim = PLAYER_ANIM_WALKING;
     } else if(tutorialCounter >= 137 && tutorialCounter <= 350) {
       DrawText("Os itens deixam de ficar verdes e podem ser chutados \n                 com o  SPACE (|---|) do teclado.", 310, 265, 23, MAROON);
        if(tutorialCounter >= 150 && tutorialKick) {
	  tutorialProp->kicked = true;
	  PlaySound(fxKick);
	  player.currentAnim = PLAYER_ANIM_KICK;
	  populationScore++;
	  kickRingBarStartAngle -= 100.0f;
	  tutorialKick = false;
	}
	if(tutorialCounter >= 172)
	  player.currentAnim = PLAYER_ANIM_WALKING;
     } else if(tutorialCounter >= 351 && tutorialCounter <= 470) {
       DrawText("CUIDADO! As garrafas de vidro não podem ser chutadas.", 280, 275, 23, MAROON);
       if(tutorialBottle == NULL) {
	 tutorialBottle = getBottle();
	 tutorialBottle->collider.rect.x = 1280/2 - 50;
	 tutorialBottle->position.x = 1280/2;
	 tutorialBottle->disabled = false;
	 tutorialBottle->alreadyColored = true;
	 tutorialBottle->colorCount = 150.0f;
       }
     }else if(tutorialCounter >= 371 && tutorialCounter <= 640) {
       DrawText("Você recebe -10 pontos se as garrafas NÃO estiverem mais verdes.", 265, 275, 23, MAROON);
     }else if(tutorialCounter >= 641 && tutorialCounter <= 790) {
       DrawText("Aumente o número de pessoas educadas.", 260, 275, 23, MAROON);
       DrawRectangleLines(0, 0, 182, 130, RED);
       tutorialScoreCounter++;
       if(tutorialScoreCounter >= 12) {
	 populationScore++;
	 tutorialScoreCounter = 0;
       }
     }else if(tutorialCounter >= 791 && tutorialCounter <= 950) {
       DrawText("Carregue a barra de chutes.", 700, 410, 23, MAROON);
       DrawRectangleLines(1280 - 220, 720 - 160, 180, 180, RED);
     }else if(tutorialCounter >= 951 && tutorialCounter <= 1650) {
       if(tutorialCounter <= 1149)
	 DrawText("Carregue a barra no máximo para andar para a direita (->).", 260, 275, 23, MAROON);
       DrawRectangleLines(1280 - 220, 720 - 160, 180, 180, RED);
       if(tutorialCounter <= 990) {
	 tutorialBottle->disabled = true;
	 player.position.x = -650;
	 kickRingBarStartAngle = 360.0f;
       }
       if(tutorialCounter >= 1050) {
	 player.movimentDirection = PLAYER_RIGHT_MOVIMENT;
	 kickRingBarStartAngle = 0.0f;
       }
       if(tutorialCounter >= 1150)
	 DrawText("Pressione SPACE (|---|) para mudar a direção.", 270, 275, 23, MAROON);
     }else if(tutorialCounter >= 1651 && tutorialCounter <= 2650) {
       DrawText("Se atente as luzes do poste, elas avisam onde irá cair o próximo item.", 265, 275, 23, MAROON);
       if(tutorialCounter >= 1660 && tutorialCounter <= 2000) {
	 backgroundFrame = 1;
	 DrawRectangleLines(20, 500, 400, 100, RED);
       } else if(tutorialCounter >= 2001 && tutorialCounter <= 2400) {
	 backgroundFrame = 2;
	 DrawRectangleLines(440, 500, 400, 100, RED);
       } else if(tutorialCounter >= 2401 && tutorialCounter <= 2590) {
	 backgroundFrame = 3;
	 DrawRectangleLines(860, 500, 400, 100, RED);	 
       } else {
	 backgroundFrame = 0;
       }
       if(backgroundFrame > 0) backgroundCount--;
       if(backgroundCount <= 0.0f) {
	 Prop *prop = getProp();
	 if(prop != NULL) {
	   int rdPos = getRandomSpawnPosX(backgroundFrame);
	   prop->collider.rect.x = rdPos + 20;
	   prop->position.x = rdPos;
	   prop->disabled = false;
	   prop->alreadyColored = true;
	   prop->colorCount = 100.0f;
	 }
	 backgroundFrame = 0;
	 backgroundCount = gameSpeed*0.7;     
       }
     } else { 
       resetGame(&player, &delayFxWheels, &gameMusicSpeed, &kickRingBarStartAngle, &bottleCount);
       tutorialCounter = 0;
       GuiSetStyle(DEFAULT, TEXT_SIZE, 17);
       tutorialProp->disabled = true;
       showTutorial = false;
     }
  }
   // Pause menu
   if(pause && !showStory && !showTutorial && !showCredits) {
     DrawRectangleRec((Rectangle){0, 0, 1280, 720}, (Color){17, 17, 17, 230});
     DrawTextureEx(menuTexture, (Vector2){0, 0}, 0.0f, 1.0f, WHITE);     
     if(GuiButton((Rectangle){500, 310, 300, 50}, "JOGAR")) pause = !pause;
     if(GuiButton((Rectangle){500, 390, 300, 50}, "LIGAR/DESLIGAR AUDIO")) pauseAudio = !pauseAudio;
     if(GuiButton((Rectangle){500, 500, 300, 50}, "SAIR")) break;
     if(GuiButton((Rectangle){30, 720 - 60, 150, 40}, "CRÉDITOS")) showCredits = !showCredits;	  
   }
   // Colliders DEBUG
   /*
   Color debugColor = {0, 228, 48, 60};
   for(int i = 0; i < PROPS_LENGTH; i++)
     if(!props[i].disabled && !props[i].kicked)
       DrawRectangleRec(props[i].collider.rect, debugColor); 
   DrawRectangleRec(player.collider.rect, debugColor);
   // Spawn DEBUG
   DrawRectangleRec((Rectangle){20, 20, 400, 50}, debugColor); 
   DrawRectangleRec((Rectangle){440, 20, 400, 50}, debugColor); 
   DrawRectangleRec((Rectangle){860, 20, 400, 50}, debugColor);  
   DrawText(TextFormat("bottleCount: %f", bottleCount), 500, 40, 40, ORANGE);
   */
   EndDrawing();
  }
  UnloadSound(fxWheels);
  UnloadSound(fxOkay);
  UnloadSound(fxError);
  UnloadSound(fxGameSpeedUp);
  UnloadSound(fxGameOver);
  UnloadSound(fxKick);
  UnloadSound(fxAuuu);
  UnloadMusicStream(musicCity);
  UnloadMusicStream(music);
  CloseAudioDevice(); 
  UnloadTexture(canTexture);
  UnloadTexture(cardboardBoxTexture);
  UnloadTexture(bottleTexture);
  UnloadTexture(playerAnim);
  UnloadTexture(floorTexture);
  UnloadTexture(populationIconTexture);
  UnloadTexture(rightIconTexture);
  UnloadTexture(menuTexture);
  UnloadTexture(backgroundTexture);
  UnloadTexture(backgroundLightLeftTexture);
  UnloadTexture(backgroundLightCenterTexture);
  UnloadTexture(backgroundLightRightTexture);
  free(props);    
  free(scoreInfos);    
  CloseWindow();        
  return 0;
}

bool isColliding(Collider *colA, Collider *colB) {
  if (colB->rect.x < colA->rect.x + colA->rect.width && 
      colB->rect.x + colB->rect.width > colA->rect.x &&
      colB->rect.y < colA->rect.y + colA->rect.height &&
      colB->rect.y + colB->rect.height > colA->rect.y)
    return true;
  else return false;
}

void UpdatePlayer(Player *player, Prop *props, int propsLength, float delta, Sound fxOkay, Sound fxError, Sound fxKick, Sound fxAuuu, float *gameMusicSpeed, float *kickRingBarStartAngle) {
  if(((IsKeyDown(KEY_SPACE) || IsKeyPressed(KEY_SPACE)) || mouseOrTouchKickButton()) && player->changeDirection) {
    player->movimentDirection *= -1;
    *kickRingBarStartAngle -= 300.0f;
    player->changeDirection = false;
  }
  if(player->movimentDirection == PLAYER_RIGHT_MOVIMENT) {
    if (IsKeyDown(KEY_RIGHT) || mouseOrTouchMoveButton()) {
      player->currentAnim = PLAYER_ANIM_WALKING;
      player->position.x += PLAYER_HOR_SPD*player->speed*delta;
    } else {
      player->currentAnim = PLAYER_ANIM_STOPED;
    }
    if(player->position.x >= 1380.0f) player->position.x = -560.0f;
  } else if(player->movimentDirection == PLAYER_LEFT_MOVIMENT) {
    if (IsKeyDown(KEY_LEFT) || mouseOrTouchMoveButton()) {
      player->currentAnim = PLAYER_ANIM_WALKING;	   
      player->position.x -= PLAYER_HOR_SPD*player->speed*delta;
    } else {
      player->currentAnim = PLAYER_ANIM_STOPED;
    }
    if(player->position.x <= -300.0f) player->position.x = 1280.0f;
  }  
  if(player->speedBonus) {
    if(player->speed < 6.8f){
      player->speed += 0.75f;
    }
    player->speedBonus = false;
  }
  for(int i = 0; i < PROPS_LENGTH; i++) {
    Collider *col = &props[i].collider;
    int scoreInfoType = 0;
    Vector2 scoreInfoPosition;
    if(!props[i].disabled && !props[i].kicked)
      if (isColliding(&player->collider, col)) {
	 if(props[i].colorCount > 0.0f) {
	  if(!pauseAudio) PlaySound(fxOkay);
	  player->speedBonus = true;
    	  populationScore++;
	  scoreInfoType = SCORE_INFO_MORE;
	  scoreInfoPosition = props[i].position;
	  if(*kickRingBarStartAngle <= 370.0f) *kickRingBarStartAngle += 60.0f;
	 } else {
	   if(((IsKeyDown(KEY_SPACE) || IsKeyPressed(KEY_SPACE)) || mouseOrTouchKickButton())
	      && player->kick && props[i].type != PROP_BOTTLE) {
	     props[i].kicked = true;
	     if(!pauseAudio) PlaySound(fxKick);
	     player->currentAnim = PLAYER_ANIM_KICK;
	     scoreInfoType = SCORE_INFO_MORE;
	     populationScore++;
	     scoreInfoPosition = props[i].position;
	     *kickRingBarStartAngle -= 100.0f;
	     if(*kickRingBarStartAngle <= 0.0f) *kickRingBarStartAngle = 10.0f;
	   } else {
	     if(gameSpeed < 300.0f) {
	       if(*gameMusicSpeed > 1.0f) *gameMusicSpeed -= 0.03f;
	       speedFeedback(player, SCORE_INFO_SLOWER);
	       gameSpeed += 50.0f;
	     }
	     player->speed = 3.0f;
	     player->speedBonus = false;
	     if(!pauseAudio) PlaySound(fxError);	 
	     if(populationScore >= 0) {
	       if(props[i].type == PROP_BOTTLE) {
		 scoreInfoType = SCORE_INFO_LESS_TEN;
		 if(!pauseAudio) PlaySound(fxAuuu);
		 populationScore -= 10;
	       } else {
		 populationScore--;
		 scoreInfoType = SCORE_INFO_LESS;
	       }
	       scoreInfoPosition = props[i].position;
	     }
	   }
	 }
	 ScoreInfo *scoreInfo = getScoreInfo();
	 if(scoreInfo != NULL) {
	   scoreInfo->type = scoreInfoType;
	   scoreInfo->position = scoreInfoPosition;
	   scoreInfo->disabled = false;
	 }
	 if(!props[i].kicked) props[i].disabled = true;
      }
  }
}

void UpdateCameraCenter(Camera2D *camera, Player *player, float delta, int width, int height) {
  camera->offset = (Vector2){ width/2.0f, height/2.0f };
  camera->target = player->position;
}

Prop *getProp () {
  for(int i = 0; i < PROPS_LENGTH; i++) {
    Prop *prop = &props[i];
    if(prop->disabled && !prop->kicked && prop->type != PROP_BOTTLE)
      return prop;
  }
  return NULL;
}

Prop *getBottle () {
  for(int i = 0; i < PROPS_LENGTH; i++) {
    Prop *prop = &props[i];
    if(prop->disabled && !prop->kicked && prop->type == PROP_BOTTLE)
      return prop;
  }
  return NULL;
}

void resetGame(Player *player, float *delayFxWheels, float *gameMusicSpeed, float *kickRingBarStartAngle, float *bottleCount) {
  if(props != NULL)
    for(int i = 0; i < PROPS_LENGTH; i++) {
      Prop *prop = &props[i];
      int posY = 0;
      if(prop->type == PROP_CAN || prop->type == PROP_BOTTLE) posY = 565; else posY = 525; 
      prop->disabled = true;
      prop->kicked = false;
      prop->collider.rect.y = posY + 10 + SCENERY_HIGHT;
      prop->position.y = posY + SCENERY_HIGHT;
      prop->alreadyColored = true;
      prop->colorCount = 100.0f;
    }
  *delayFxWheels = 200.0f;
  *gameMusicSpeed = 1.0f;
  *bottleCount = 0.0f;
  gameSpeed = 300.0f;
  gameCount = 3;
  populationScore = 0;
  player->position = (Vector2){ 400, 280 };
  player->speed = 3;
  player->kick = false;
  player->changeDirection = false;
  player->movimentDirection = PLAYER_LEFT_MOVIMENT;
  player->currentAnim = PLAYER_ANIM_STOPED;
  Collider playerCollider = {
    false,
    {player->position.x + 30, player->position.y + 130, 120, 360},
    false
  };
  *kickRingBarStartAngle = 0.0f;
  player->collider = playerCollider;
  player->speedBonus = false;
}

ScoreInfo *getScoreInfo() {
  for(int i = 0; i < SCORE_INFO_LENGTH; i++) {
    ScoreInfo *scoreInfo = &scoreInfos[i];
    if(scoreInfo->disabled)
      return scoreInfo;
  }
  return NULL;
}

int getRandomSpawnPosX(int backgroundFrame) {
  int rdPos;
  if(backgroundFrame == 1)
    rdPos = 20 + (rand() % 401);
  else if(backgroundFrame == 2)
    rdPos = 440 + (rand() % 401);
  else
    rdPos = 860 + (rand() % 401);
  return rdPos;
}

Rectangle flipHRect(Rectangle rect) {
  return (Rectangle) {rect.x, rect.y, rect.width*-1, rect.height};
}

bool isCollidingKickButton(Vector2 pos) {
  return isColliding(&(Collider){false, {pos.x , pos.y, 50, 50}, true},
		     &(Collider){false, {1280 - 220, 720 - 160, 180, 180}, true});
}
bool isCollidingMoveButton(Vector2 pos) {
  return isColliding(&(Collider){false, {pos.x , pos.y, 50, 50}, true},
		     &(Collider){false, {0 + 40, 720 - 126, 180, 180}, true});
}
bool mouseOrTouchKickButton() {
  Vector2 touchPosition = GetTouchPosition(1);
  Vector2 mousePosition = GetMousePosition();
  return (isCollidingKickButton(touchPosition) || isCollidingKickButton(mousePosition));
}
bool mouseOrTouchMoveButton() {
  Vector2 touchPosition = GetTouchPosition(1);
  Vector2 mousePosition = GetMousePosition();
  return (isCollidingMoveButton(touchPosition) || isCollidingMoveButton(mousePosition));
}

void speedFeedback(Player *player, int feedbackType) {
  ScoreInfo *scoreInfo = getScoreInfo();
  if(scoreInfo != NULL) {
    scoreInfo->type = feedbackType;
    if(player->movimentDirection == PLAYER_RIGHT_MOVIMENT)
      scoreInfo->position = (Vector2){player->position.x + 420, player->position.y + 120};
    else
      scoreInfo->position = (Vector2){player->position.x, player->position.y + 120};
    scoreInfo->disabled = false;
  }
}

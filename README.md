# O Catador

##### Acesse o link abaixo para jogá-lo no navegador:

https://diogohartuiqdebarba.gitlab.io/o-catador

##### ou baixe para linux:

[O Catador - Linux x86_64](build/linux/o-catador-linux-x86_64.zip)

##### ou baixe para windows:

[O Catador - Windows x86_64](build/windows/o-catador-windows-x86_64.zip)

![O Catador - História](img/menu-story.png)

### Controles:

**< - >** Use as setas para mover. Porém só é permitido se mover para um dos lados de cada vez, e tem que usar o bônus de
mudar de direção para começar a andar para o outro lado.

**|---|** Use SPACE para chutar os itens ou para usar o bônus de mudar de direção.

Você também pode utilizar os botões de movimento e "chute/mudar direção" com o touch ou o mouse.

#### Brainstorm

[Brainstorm - O Catador](documents/Reciclagem.pdf)

#### Planejamento e gerenciamento do projeto com o Trello (Kanban)

[Gerencimento de projeto - O Catador](https://trello.com/b/RkN1nSIz/o-catador)

#### GDD:

[GDD - O Catador](documents/GDD%20-%20O%20catador%20-%20Diogo%20Hartuiq%20Debarba.pdf)

#### Todo o processo de criação foi gravado e está no Youtube

[Playlist do processo de criação - O Catador](https://youtube.com/playlist?list=PLD4GWWO8N31bUcM4PHSrhAKGXc9nCgKha)


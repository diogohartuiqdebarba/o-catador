!#/bin/bash

# -------------------
#       LINUX
# -------------------
make src/main PLATFORM=PLATFORM_DESKTOP RAYLIB_PATH=~/Programs/raylib2
#gcc -o src/main src/main.c -Wall -std=c99 -D_DEFAULT_SOURCE -Wno-missing-braces -s -O1 -D_DEFAULT_SOURCE -I/usr/local/include -I. -I/home/noloop/Programs/raylib/src -I/home/noloop/Programs/raylib/src/external -L. -L/usr/local/lib -L/home/noloop/Programs/raylib/src -L/home/noloop/Programs/raylib -lraylib -lGL -lm -lpthread -ldl -lrt -lX11 -DPLATFORM_DESKTOP
mv src/main ./build/linux/$1
cd ./build/linux/
mkdir -p o-catador-linux-x86_64/
cp -r ../../assets/ o-catador-linux-x86_64/
cp o-catador o-catador-linux-x86_64/
zip -r $1-linux-x86_64.zip o-catador-linux-x86_64/
rm -r o-catador-linux-x86_64/
#cd ../../
#./build/linux/$PROJECT_NAME

# -------------------
#      WINDOWS
# -------------------
#x86_64-w64-mingw32-gcc -c src/main.c -o build/windows/main.o -I/home/noloop/Programs/raylib/src -L/home/noloop/Programs/raylib/src -I/home/noloop/Programs/raygui/src -lm -lraylib -pthread -lopengl32 -lgdi32 -lwinmm -mwindows
#x86_64-w64-mingw32-gcc build/windows/main.o -o build/windows/$1.exe -L/home/noloop/Programs/raylib/src -I/home/noloop/Programs/raygui/src -lm -lraylib -pthread -lopengl32 -lgdi32 -lwinmm -mwindows
#./build/windows/$1.exe

# -------------------
#        WEB 
# -------------------
# cd ~/Programs/raylib/src
#make PLATFORM=PLATFORM_WEB -B
#./home/noloop/Programs/emsdk/emsdk activate latest
#source ~/Programs/emsdk/emsdk_env.sh
#make src/main PLATFORM=PLATFORM_WEB -B RAYLIB_PATH=~/Programs/raylib
#emcc -o build/web/index.html src/main.c -Wall -std=c99 -D_DEFAULT_SOURCE -Wno-missing-braces -Os -s USE_GLFW=3 -s ASYNCIFY -s TOTAL_MEMORY=67108864 -s FORCE_FILESYSTEM=1 --preload-file assets --shell-file /home/noloop/Programs/raylib/src/shell.html -I. -I/home/noloop/Programs/raylib/src -I/home/noloop/Programs/raylib/src/external -I/home/noloop/Programs/raygui/src -L. -L/home/noloop/Programs/raylib/src /home/noloop/Programs/raylib/src/libraylib.a -DPLATFORM_WEB
